from src.api_client import ApiClient

def Send(subject, EEfrom, fromName, to, bodyHtml, bodyText, isTransactional, attachmentFiles=[]):
    tmp_attachments = []
    for name in attachmentFiles:
        tmp_attachments.append(('attachments', open(name, 'rb')))

    return ApiClient.Request('POST', '/email/send', {
        'subject': subject,
        'from': EEfrom,
        'fromName': fromName,
        'to': to,
        'bodyHtml': bodyHtml,
        'bodyText': bodyText,
        'isTransactional': isTransactional
    }, tmp_attachments)
