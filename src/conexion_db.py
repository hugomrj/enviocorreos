import mysql.connector

# Configura los parámetros de conexión a la base de datos
db_config = {
    "host": "localhost",
    "user": "root",
    "password": "admin",
    "database": "mecsueldos",
    "port": 3308
}

# Función para establecer la conexión a la base de datos
def establecer_conexion():
    try:
        connection = mysql.connector.connect(**db_config)
        return connection
    except mysql.connector.Error as err:
        print(f"Error de MySQL al conectarse a la base de datos: {err}")
        return None
