from src.conexion_db import establecer_conexion

def recorrer_tabla_correos():
    connection = establecer_conexion()

    if connection is not None:
        print("Conexión exitosa a la base de datos.")
        cursor = connection.cursor()

        query = "SELECT cedula, email FROM correos"
        cursor.execute(query)

        for (cedula, email) in cursor:
            print(f"Cédula: {cedula}, Email: {email}")

        cursor.close()
        connection.close()
    else:
        print("No se pudo conectar a la base de datos. Verifica la configuración.")

if __name__ == '__main__':
    recorrer_tabla_correos()
