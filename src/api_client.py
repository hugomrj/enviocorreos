import requests

class ApiClient:
    
    apiUri = 'https://api.elasticemail.com/v2'
    apiKey = '79F8ED166058DD001ACB66B79AB844AC6888AC9E9D6DA07D1F5FBAE29A662D7C7CFCC1F03B29E2B0C885BEB17BA718A3'

    @staticmethod
    def Request(method, url, data, attachs=None):
        data['apikey'] = ApiClient.apiKey
        if method == 'POST':
            result = requests.post(ApiClient.apiUri + url, params=data, files=attachs)
        elif method == 'PUT':
            result = requests.put(ApiClient.apiUri + url, params=data)
        elif method == 'GET':
            attach = ''
            for key in data:
                attach = attach + key + '=' + data[key] + '&'
            url = url + '?' + attach[:-1]
            result = requests.get(ApiClient.apiUri + url)

        jsonMy = result.json()

        if jsonMy['success'] is False:
            return jsonMy['error']

        return jsonMy['data']
