CREATE TABLE `correos` (
  `cedula` int NOT NULL,
  `email` varchar(120) DEFAULT NULL,
  `enviar` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;