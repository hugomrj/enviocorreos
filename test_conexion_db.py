from src.conexion_db import establecer_conexion
from src.recorrer_correos import recorrer_tabla_correos

def main():
    
    connection = establecer_conexion()
    
    if connection is not None:
        print("Conexión exitosa a la base de datos.")
        # Realiza operaciones de prueba con la conexión
        # ...

        # Cierra la conexión cuando hayas terminado
        connection.close()
    else:
        print("No se pudo conectar a la base de datos. Verifica la configuración.")


if __name__ == '__main__':
#    main()
    recorrer_tabla_correos()
