import argparse
from src.email_sender import Send
from src.conexion_db import establecer_conexion

if __name__ == '__main__':
    
    subject = "certificado de sueldos"
    EEfrom = "dasp@mec.gov.py"
    fromName = "ministerio de educación"
    bodyHtml = "envío de prueba        "
    bodyText = ""
    isTransactional = True

    # Establece la conexión a la base de datos
    connection = establecer_conexion()

    # Argumentos para el mes y el año
    parser = argparse.ArgumentParser()
    parser.add_argument("--mes", help="Mes en formato YYYYMM", required=True)
    parser.add_argument("--anio", help="Año en formato YYYY", required=True)    
    args = parser.parse_args()



    if connection is not None:
        cursor = connection.cursor()

        # Ejecuta una consulta SQL para recuperar los correos y cédulas
        query = "SELECT email, cedula FROM correos where enviar = 'si'"
        cursor.execute(query)

        # Recorre los resultados y obtén los correos y cédulas
        for row in cursor:
            
            email = row[0]  # Obtiene el correo de la fila actual
            cedula = row[1]  # Obtiene la cédula de la fila actual

            # valores del mes y el año
            mes = args.mes
            anio = args.anio

            # Construye el nombre del archivo PDF a partir de la cédula, mes y año
            pdf_filename = f'Z:/estractosueldos/{cedula}_{anio}{mes}.pdf'

            # Imprime el valor de 'email' y 'pdf_filename' para visualizarlo
            print(f"Enviando correo a: {email}")
            print(f"Nombre del archivo PDF: {pdf_filename}")

            # Envia el correo a la dirección actual con el archivo PDF correspondiente
            result = Send(subject, EEfrom, fromName, email, bodyHtml, bodyText, isTransactional, [pdf_filename])
            print(f"Resultado: {result}")

        cursor.close()
        connection.close()

    else:
        print("No se pudo conectar a la base de datos. Verifica la configuración.")
